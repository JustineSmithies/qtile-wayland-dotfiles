# Various dotfiles for my [Void](https://voidlinux.org/) - [Qtile-Wayland](https://github.com/qtile/qtile) setup.
A full-featured, hackable tiling window manager written and configured in Python.
Please note that I use the latest Qtile from their GitHub repo which can be installed by following the installation instructions [below](#installing-qtile-wayland).

![ScreenShot](screenshot.jpg)

## Details

Below is a list of some of the packages that I use for my current setup which consists of two screens. A monitor as the main screen and the laptop screen as a slave and Kanshi is used to set the monitor as the left most. Please note that my config isn't perfect so your mileage may vary.<br />
The wallpaper featured in the screenshots can be found here: https://cdna.artstation.com/p/assets/images/images/015/554/914/large/artur-sadlos-to-sh300-ooh-as-05i.jpg
<br />
- **Operating System** --- [Void](https://voidlinux.org/)
- **Boot Loader** --- [Grub](https://www.gnu.org/software/grub/index.html)
- **Resource Monitor** --- [Btop](https://github.com/aristocratos/btop)
- **Window Manager** --- [Qtile](https://github.com/qtile/qtile)
- **Status Bar** --- [Qtile's own](https://codeberg.org/qtile/qtile)
- **Screen Detection** --- [wlr-randr](https://sr.ht/~emersion/wlr-randr/)
- **Screen Hotplug** --- [Kanshi](https://sr.ht/~emersion/kanshi/)
- **Screen Locker** --- [Swaylock](https://github.com/swaywm/swaylock)
- **Screenshots** --- [Grim](https://sr.ht/~emersion/grim/)
                  --- [Slurp](https://github.com/emersion/slurp)
- **Idle Management Daemon** --- [Swayidle](https://github.com/swaywm/swayidle)
- **Shell** --- [Bash](https://www.gnu.org/software/bash/) using [Starship](https://starship.rs/) 
- **Completion** --- [Bash Completion](https://github.com/scop/bash-completion)
- **Terminal** --- [Foot](https://codeberg.org/dnkl/foot)
- **Notification Daemon** --- [Dunst](https://github.com/dunst-project/dunst)
- **Application Launcher** --- [Fuzzel](https://codeberg.org/dnkl/fuzzel)
- **File Manager** --- [Ranger](https://github.com/ranger/ranger)
- **Image Viewer** --- [Imv](https://git.sr.ht/~exec64/imv/)
- **Document Scanner** -- [Simple-scan](https://github.com/GNOME/simple-scan)
- **Editor** --- [Neovim](https://github.com/neovim/neovim)
  - **Plugins**
  	- [packer](https://github.com/wbthmason/packer.nvim)
	- [gruvbox](https://github.com/ellisonleao/gruvbox.nvim)
	- [nvim-tree.lua](https://github.com/kyazdani42/nvim-tree.lua)
	- [nvim-web-devicons](https://github.com/kyazdani42/nvim-web-devicons)
	- [mason.nvim](https://github.com/williamboman/mason.nvim)
	- [mason-lspconfig.nvim](https://github.com/williamboman/mason-lspconfig.nvim)
	- [nvim-lspconfig](https://github.com/neovim/nvim-lspconfig)
	- [lualine.nvim](https://github.com/nvim-lualine/lualine.nvim)
	- [nvim-treesitter](https://github.com/nvim-treesitter/nvim-treesitter)
	- [nvim-colorizer.lua](https://github.com/norcalli/nvim-colorizer.lua)
	- [tagbar](https://github.com/preservim/tagbar)
	- [alpha-vim](https://github.com/goolord/alpha-vim)
	- [alpha-nvim-fortune](https://github.com/BlakeJC94/alpha-nvim-fortune)
	- [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
	- [cmp-nvim-lsp](https://github.com/hrsh7th/cmp-nvim-lsp)
	- [luasnip](https://github.com/L3MON4D3/Luasnip)
	- [cmp-nvim-lua](https://github.com/hrsh7th/cmp-nvim-lua)
	- [cmp-buffer](https://github.com/hrsh7th/cmp-buffer)
	- [cmp-path](https://github.com/hrsh7th/cmp-path)
	- [cmp-cmdline](https://github.com/hrsh7th/cmp-cmdline)
	- [telescope.nvim](https://github.com/nvim-telescope/telescope.nvim)
	- [plenary.nvim](https://github.com/nvim-lua/plenary.nvim)
	- [indent-blankline.nvim](https://github.com/lukas-reineke/indent-blankline.nvim)
	- [undotree](https://github.com/jiaoshijie/undotree)
	- [Comment.nvim](https://github.com/numToStr/Comment.nvim)
	- [lsp_lines.nvim](https://git.sr.ht/~whynothugo/lsp_lines.nvim)
    - [bufdelete.nvim](https://github.com/famiu/bufdelete.nvim)
- **Web Browser** --- [Qutebrowser](https://www.qutebrowser.org)
- **Multimedia Framework**
	- **PipeWire**
		- [pipeWire](https://pipewire.org/)
		- [alsa-pipewire](https://pipewire.org/)
		- [libjack-pipewire](https://pipewire.org/)
		- [wireplumber](https://gitlab.freedesktop.org/pipewire/wireplumber/)
		- [libpulseaudio](https://www.freedesktop.org/wiki/Software/PulseAudio/)
- **PDF Viewer** --- [Zathura](https://git.pwmt.org/pwmt/zathura)
- **IRC** --- [Weechat](https://weechat.org/)
- **RSS Feed Reader** --- [Newsboat](https://newsboat.org/)
- **Youtube Downloader** --- [Yt-dlp (youtube-dl fork)](https://github.com/yt-dlp/yt-dlp)
- **Video player** --- [mpv](https://github.com/mpv-player/mpv)
- **Email Client** 
    - [aerc](https://git.sr.ht/~rjarry/aerc)
    - [isync](https://isync.sourceforge.io/)
    - [msmtp](https://marlam.de/msmtp/)
    - [dante](https://www.inet.no/dante/)
    - [w3m](https://w3m.sourceforge.net/)
    - [msmtpqueue](https://github.com/marlam/msmtp-mirror/tree/master/scripts/msmtpqueue)


## Installing Qtile Wayland

#### 1. Ensure the wlroots is installed

On Void: `sudo xbps-install -S wlroots0.16 wlroots0.16-devel`

#### 2. Clone the Qtile repo

`git clone https://github.com/qtile/qtile.git
cd qtile`

#### 3. Install Qtile dependencies

`python -m pip install dbus-next pywlroots pywayland "cairocffi >= 1.6.0" "cffi >= 1.1.0" "xcffib >= 1.4.0" wheel --no-cache-dir --force`

#### 4. Install qtile for the first time

`python -m pip install .`

#### 5. Find FFI Names

The following command lists the names of the system binaries we need copy

`ls /usr/lib64 | grep "libxkbcommon\|libinput\|libwlroots"`

The following command lists the names of the pywlroots binaries we need to replace

`ls ~/.local/lib/python3.11/site-packages/pywlroots.libs | grep "libinput\|li bxkbcommon\|libwlroots"
`
#### 6. Patch the pywlroots FFI

Symlink the relevant system binary to the pywlroot binaries. In my case

```
ln /usr/lib64/libxkbcommon.so.0.0.0 ~/.local/lib/python3.11/site-packages/pywlroots.libs/libxkbcommon-de59cad2.so.0.0.0 -fs
ln /usr/lib64/libinput.so.10.13.0 ~/.local/lib/python3.11/site-packages/pywlroots.libs/libinput-a3c39512.so.10.13.0 -fs
ln /usr/lib64/libwlroots.so.11 ~/.local/lib/python3.11/site-packages/pywlroots.libs/libwlroots-62d802e9.so.11 -fs
```

#### 7. Build qtile FFI
This next command is super important, we need to build the ffi for the virtual enviroments qtile

```
cd ~/.local/lib/python3.11/site-packages
python libqtile/backend/wayland/cffi/build.py
```

#### 8. Install Qtile for the second and (hopefully) final time

```
cd 'The location of your qtile git repo'/qtile
python -m pip install .
```

#### 9. Start Qtile

Either:

`qtile start -b wayland`

Or:

`dbus-run-session qtile start -b wayland`

## Keybindings
### Window manager controls
| Keys                                 | Action                    |
| ------------------------------------ | ------------------------- |
| <kbd>CTRL + MOD + r</kbd>            | restart Qtile             |
| <kbd>CTRL + MOD + q</kbd>            | shutdown Qtile            |
| <kbd>CTRL + MOD + w</kbd>            | Open window switcher      |
| <kbd>MOD + w</kbd>                   | kill window               |
| <kbd>MOD + RETURN</kbd>              | spawn terminal            |
| <kbd>CTRL + Space</kbd>              | Rofi menu                 |
| <kbd>CTRL + MOD + p</kbd>            | Rofi power menu           |

## To set the wallpaper

From within ranger if you highlight a valid image file and enter "wp" it will set that image as the default wallpaper.
