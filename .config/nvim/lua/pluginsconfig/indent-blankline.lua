
require("indent_blankline").setup {
	enabled = false, -- Don't enable plugin at Nvim start
	show_current_context = true,
	show_current_context_start = false,
}
